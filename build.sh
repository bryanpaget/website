#!/bin/bash

header="./src/header.txt"
footer="./src/footer.txt"

for file in ./src/*.html;
do
	f=${file:5}
	cat $header $file $footer > build/"$f";
done

for file in ./src/blog/*.html;
do
	f=${file:5}
	echo $f
	cat $header $file $footer > build/"$f";
done

cp -R img style build;
cp -R src/blog build;
