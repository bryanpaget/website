<html>
  <head>
    <link rel="stylesheet" type="text/css" href="./style/style.css">
  </head>
  <div class="navigation" id="sticky">
    <div id="align-right">
      <a href="contact.html"> Contact </a>
      <a href="resume.html"> Resume </a>
    </div>
    <div id="align-left">
      <a href="blog.html"> Blog </a>
      <a href="index.html"> Home </a>
    </div>
  </div>
  <h1>Bryan Paget</h1>
  <h4>Privacy respecting, socially aware data scientist.</h4>
</div>
</div>
